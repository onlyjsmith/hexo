---
title: Hello World
thumbnail: /css/images/hello-world.jpg
---
Welcome to [Hexo](https://hexo.io/)! This is your very first post. Check [documentation](https://hexo.io/docs/) for more info. If you get any problems when using Hexo, you can find the answer in [troubleshooting](https://hexo.io/docs/troubleshooting.html) or you can ask me on [GitHub](https://github.com/hexojs/hexo/issues).

## Quick Start

### Create a new post

``` bash
$ hexo new "My New Post"
```

More info: [Writing](https://hexo.io/docs/writing.html)

### Run server

``` bash
$ hexo server
```

More info: [Server](https://hexo.io/docs/server.html)

### Generate static files

``` bash
$ hexo generate
```

More info: [Generating](https://hexo.io/docs/generating.html)

### Deploy to remote sites

``` bash
$ hexo deploy
```

More info: [Deployment](https://hexo.io/docs/deployment.html)

[Image](https://www.google.com.br/search?biw=1536&bih=752&q=dancing+in+the+rain&tbm=isch&tbs=simg:CAQSJgnPduV9U0JXExoSCxCwjKcIGgAMCxCOrv4IGgAMIeSSA8hi0Kxu&sa=X&ved=0ahUKEwiG_o3_hJ_LAhVHI5AKHcHGAw4Q2A4IICgB#imgrc=_)
